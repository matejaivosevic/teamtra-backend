﻿using Teamtra_Backend.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Teamtra_Backend.Services
{
    public interface IBlobService
    {
        IEnumerable<string> GetForCourse(string name);
        Task<BlobInfo> GetBlobAsync(string name);

        IEnumerable<string> ListBlobsAsync();

        Task UploadFileBlobAsync(string filePath, string fileName, string courseID);

        void UploadContentBlobAsync(string content, string fileName);

        Task DeleteBlobAsync(string blobName);
    }
}
