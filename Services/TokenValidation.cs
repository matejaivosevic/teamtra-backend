﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace Teamtra_Backend.Services
{
    public interface ITokenValidation
    {
        bool isValid(string req);
    }

    public class TokenValidation : ITokenValidation
    {
        public bool isValid(string req)
        {
            if (req == null)
            {
                return false;
            }
            if (req.ToString().Split(" ")[0] == "Bearer")
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var token = req.ToString().Split(" ")[1];
                var paresedToken = tokenHandler.ReadJwtToken(token);

                var role = paresedToken.Claims
                    .Where(c => c.Type == "role")
                    .FirstOrDefault();

                var profilePicturePath = paresedToken.Claims
                    .Where(c => c.Type == "profilePicturePath")
                    .FirstOrDefault();

                var email = paresedToken.Claims
                    .Where(c => c.Type == "email")
                    .FirstOrDefault();

                return email != null && role != null ? true : false;
            }
            return false;
        }
    }
}

