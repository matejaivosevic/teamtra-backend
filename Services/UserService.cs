﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Teamtra_Backend.Models;
using Teamtra_Backend.utils;

namespace Teamtra_Backend.Services
{
    public interface IUserService
    {
        Token Authenticate(string username, string password);
        User findUserByID(string id);
        User Login(string email, string password);
        Register Register(Register register);
        void Update(User user);
        bool VerificationTokenValid(string userId, string token);
    }

    public class UserService : IUserService
    {
        public SqlConnection dbConnection = DbConnection.DbConnection.GetConnection();

        public User Login(string email, string password)
        {
            string mypassword;
            using (SHA1Managed sha = new SHA1Managed())
            {
                var hashed = sha.ComputeHash(Encoding.UTF8.GetBytes(password));
                var builder = new StringBuilder(hashed.Length * 2);

                foreach (byte b in hashed)
                {
                    builder.Append(b.ToString("x2"));
                }
                mypassword = builder.ToString();
            }
            SqlCommand student = new SqlCommand(@"select * from [User] where email=@email and password=@password", dbConnection);
            dbConnection.Open();
            student.Parameters.AddWithValue("email", email);
            student.Parameters.AddWithValue("password", password);
            SqlDataReader rd = student.ExecuteReader();
            var dt = new DataTable();
            dt.Load(rd);
            dbConnection.Close();
            return Utils.ConvertDataTableToModel<User>(dt);
        }

        public User findUserByID(string id)
        {
            string query = @"select * from [User] where userID=@id";
            SqlCommand find = new SqlCommand(query, dbConnection);
            dbConnection.Open();
            find.Parameters.AddWithValue("id", id);
            SqlDataReader rdd = find.ExecuteReader();
            var dtt = new DataTable();
            dtt.Load(rdd);
            dbConnection.Close();
            return Utils.ConvertDataTableToModel<User>(dtt);
        }

        public Register Register(Register register)
        {
            string newPassword = Utils.EncryptUserPassword(register.password);
            Guid guid = Guid.NewGuid();
            Guid confirmationToken = Guid.NewGuid();
            string query = @"insert into [User] values(@guid, 'Player', @firstName, @lastName, '', '', @age, @email, @password, @city, '0', 'Beginner');
                            insert into VerificateAccount values(@guidd, @token, '')";
            SqlCommand insert = new SqlCommand(query, dbConnection);
            dbConnection.Open();
            insert.Parameters.AddWithValue("password", register.password);
            insert.Parameters.AddWithValue("guid", guid.ToString());
            insert.Parameters.AddWithValue("firstName", register.firstName);
            insert.Parameters.AddWithValue("lastName", register.lastName);
            insert.Parameters.AddWithValue("age", register.age);
            insert.Parameters.AddWithValue("email", register.email);
            insert.Parameters.AddWithValue("city", register.city);
            insert.Parameters.AddWithValue("guidd", guid.ToString());
            insert.Parameters.AddWithValue("token", confirmationToken.ToString());
            insert.ExecuteNonQuery();

            string quer = @"select * from [User] where userID=@id";
            SqlCommand inser = new SqlCommand(quer, dbConnection);
            inser.Parameters.AddWithValue("id", guid);
            SqlDataReader rdd = inser.ExecuteReader();
            var dtt = new DataTable();
            dtt.Load(rdd);
            dbConnection.Close();
            var acc = Utils.ConvertDataTableToModel<Register>(dtt);
            acc.token = confirmationToken.ToString();
            acc.regid = guid.ToString();
            return acc;
        }

        public bool VerificationTokenValid(string userId, string token)
        {
            SqlCommand verify = new SqlCommand(@"select * from VerificateAccount where userID=@ui and token=@token", dbConnection);
            dbConnection.Open();
            verify.Parameters.AddWithValue("ui", userId);
            verify.Parameters.AddWithValue("token", token);
            SqlDataReader rd = verify.ExecuteReader();
            var dt = new DataTable();
            dt.Load(rd);
            dbConnection.Close();
            var account = Utils.ConvertDataTableToModel<VerifyAccount>(dt);
            if (account == null)
            {
                return false;
            }
            VerifyAccount(userId);
            return true;
        }

        public void VerifyAccount(string userId)
        {
            SqlCommand verify = new SqlCommand(@"update [User] set isVerified = '1' where userID=@userid; delete from VerificateAccount where userID=@userid", dbConnection);
            dbConnection.Open();
            verify.Parameters.AddWithValue("userid", userId);
            verify.ExecuteNonQuery();
            dbConnection.Close();
        }

        private readonly AppSettings _appSettings;

        public UserService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public Token Authenticate(string email, string password)
        {
            var user = Login(email, password);

            if (user == null)
                return null;

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("3ce1637ed40041cd94d4f5r6ye766c4d");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim("id", user.userID),
                    new Claim("role", user.role),
                    new Claim("isVerified", user.isVerified),
                    new Claim("firstName", user.firstName),
                    new Claim("lastName", user.lastName),
                    new Claim("city", user.city),
                    new Claim("age", user.age),
                    new Claim("profilePitcurePath", user.profilePicturePath)
                }),
                Expires = DateTime.UtcNow.AddMinutes(30),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var jwtSecurityToken = tokenHandler.WriteToken(token);

            return new Token() { auth_token = jwtSecurityToken, user = user };
        }

        public void Update(User user)
        {
            string newPassword = Utils.EncryptUserPassword(user.password);
            SqlCommand query = new SqlCommand(@"update [User] set firstName = @firstName, lastName = @lastName, phoneNumber = @phoneNumber, age = @age, email = @email, password = @password, city = @city, category = @category where userID=@userid;", dbConnection);
            dbConnection.Open();
            query.Parameters.AddWithValue("firstName", user.firstName);
            query.Parameters.AddWithValue("lastName", user.lastName);
            query.Parameters.AddWithValue("phoneNumber", user.phoneNumber);
            query.Parameters.AddWithValue("age", user.age);
            query.Parameters.AddWithValue("email", user.email);
            query.Parameters.AddWithValue("password", user.password);
            query.Parameters.AddWithValue("city", user.city);
            query.Parameters.AddWithValue("category", user.category);
            query.Parameters.AddWithValue("userid", user.userID);
            query.ExecuteNonQuery();
            dbConnection.Close();
        }
    }
}
