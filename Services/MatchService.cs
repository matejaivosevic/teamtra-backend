﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Teamtra_Backend.Hubs;
using Teamtra_Backend.Models;
using Teamtra_Backend.utils;

namespace Teamtra_Backend.Services
{
    public interface IMatchService
    {
        IEnumerable<Match> FilterBySportAndDifficulty(string sport, string category, string city);
        Match findMatchById(string id);
        void CreateMatch(Match match);
        Match UpdateMatch(Match match);
        IEnumerable<Match> AddUserToMatch(string id, string userId);
        IEnumerable<Match> CancelQueue(string id, string userId);
        IEnumerable<UserPlaysMatch> AppliedMatches(string userid);
    }
    public class MatchService : IMatchService
    {
        public SqlConnection dbConnection = DbConnection.DbConnection.GetConnection();

        public IEnumerable<Match> FilterBySportAndDifficulty(string sport, string category, string city)
        {
            string query = @"select * from [Match] where sport=@sport and category=@category and city=@city";
            SqlCommand find = new SqlCommand(query, dbConnection);
            dbConnection.Open();
            find.Parameters.AddWithValue("sport", sport);
            find.Parameters.AddWithValue("category", category);
            find.Parameters.AddWithValue("city", city);
            SqlDataReader rdd = find.ExecuteReader();
            var dtt = new DataTable();
            dtt.Load(rdd);
            dbConnection.Close();
            return Utils.ConvertDataTableToList<Match>(dtt);
        }

        public IEnumerable<UserPlaysMatch> AppliedMatches(string userid)
        {
            string query_ = @"select matchID from UserPlaysMatch where userID=@id";
            SqlCommand find_ = new SqlCommand(query_, dbConnection);
            dbConnection.Open();
            find_.Parameters.AddWithValue("id", userid);
            SqlDataReader rdd_ = find_.ExecuteReader();
            var dtt_ = new DataTable();
            dtt_.Load(rdd_);
            dbConnection.Close();
            return Utils.ConvertDataTableToList<UserPlaysMatch>(dtt_);
        }

        public Match findMatchById(string id)
        {
            string query = @"select * from [Match] where matchID=@id";
            SqlCommand find = new SqlCommand(query, dbConnection);
            dbConnection.Open();
            find.Parameters.AddWithValue("id", id);
            SqlDataReader rdd = find.ExecuteReader();
            var dtt = new DataTable();
            dtt.Load(rdd);
            dbConnection.Close();
            return Utils.ConvertDataTableToModel<Match>(dtt);
        }

        public void CreateMatch(Match match)
        {
            Guid guid = Guid.NewGuid();
            string query = @"insert into Match values(@id, @name, @place, @startingAt, '', @isPublic, @desc, @reqN, @avaN, @owner, null, @city, @category, @sport, '0')";
            SqlCommand create = new SqlCommand(query, dbConnection);
            dbConnection.Open();
            create.Parameters.AddWithValue("id", guid.ToString());
            create.Parameters.AddWithValue("name", match.name);
            create.Parameters.AddWithValue("place", match.place);
            create.Parameters.AddWithValue("startingAt", match.startingAt);
            create.Parameters.AddWithValue("isPublic", match.isPublic);
            create.Parameters.AddWithValue("desc", match.description);
            create.Parameters.AddWithValue("reqN", match.requiredNumberOfPlayers);
            create.Parameters.AddWithValue("avaN", match.availableNumberOfPlayers);
            create.Parameters.AddWithValue("owner", match.matchOwnerID);
            create.Parameters.AddWithValue("city", match.city);
            create.Parameters.AddWithValue("category", match.category);
            create.Parameters.AddWithValue("sport", match.sport);
            create.ExecuteNonQuery();
            dbConnection.Close();
        }

        public Match UpdateMatch(Match match)
        {
            string MatchJson = Newtonsoft.Json.JsonConvert.SerializeObject(match);
            string query = @"update Match set Match.name = isnull(json.name, Match.name),
                Match.iPublic = isnull(json.isPublic, Match.isPublic),
                Match.description = isnull(json.description, Match.description),
                Match.requiredNumberOfPlayers = isnull(json.requiredNumberOfPlayers, Match.requiredNumberOfPlayers),
                Match.availableNumberOfPlayers = isnull(json.availableNumberOfPlayers, Match.availableNumberOfPlayers),
                Match.winnerID = isnull(json.winnerID, Match.winnerID),
                Match.category = isnull(json.category, Match.category),
                Match.queueNumber = isnull(json.queueNumber, Match.queueNumber),
                Match.place = isnull(json.place, Match.place),
                Match.startingAt = isnull(json.startingAt, Match.startingAt) from OPENJSON(@matchJson) with
                (MatchID nvarchar(500), name nvarchar(100), place nvarchar(100),
                startingAt datetime, endingAt datetime, isPublic bit, description nvarchar(200),
                requiredNumberOfPlayers int, availableNumberOfPlayers int,
                matchOwnerID nvarchar(100), winnerID nvarchar(100), city nvarchar(50), category nvarchar(50),
                sport varchar(50), queueNumber(50)) as json where matchID=@id";
            SqlCommand update = new SqlCommand(query, dbConnection);
            dbConnection.Open();
            update.Parameters.AddWithValue("matchJson", MatchJson);
            update.Parameters.AddWithValue("id", match.matchID);
            update.ExecuteNonQuery();
            dbConnection.Close();
            return findMatchById(match.matchID);
        }

        public IEnumerable<Match> AddUserToMatch(string id, string userId)
        {
            string query = @"insert into UserPlaysMatch values(@userid, @id); update [Match] set queueNumber=queueNumber+1 where matchID=@idd";
            SqlCommand update = new SqlCommand(query, dbConnection);
            dbConnection.Open();
            update.Parameters.AddWithValue("id", id);
            update.Parameters.AddWithValue("idd", id);
            update.Parameters.AddWithValue("userid", userId);
            update.ExecuteNonQuery();
            dbConnection.Close();
            Match match = findMatchById(id);
            return FilterBySportAndDifficulty(match.sport, match.category, match.city);
        }

        public IEnumerable<Match> CancelQueue(string id, string userId)
        {
            string query = @"delete from UserPlaysMatch where matchID=@id and userID=@userid; update [Match] set queueNumber=queueNumber-1 where matchID=@idd";
            SqlCommand update = new SqlCommand(query, dbConnection);
            dbConnection.Open();
            update.Parameters.AddWithValue("id", id);
            update.Parameters.AddWithValue("idd", id);
            update.Parameters.AddWithValue("userid", userId);
            update.ExecuteNonQuery();
            dbConnection.Close();
            Match match = findMatchById(id);
            return FilterBySportAndDifficulty(match.sport, match.category, match.city);
        }
    }
}
