﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Teamtra_Backend.Extensions;
using BlobInfo = Teamtra_Backend.Models.BlobInfo;

namespace Teamtra_Backend.Services
{
    public class BlobService : IBlobService
    {
        private readonly BlobServiceClient _blobServiceClient;
        public SqlConnection dbConnection = DbConnection.DbConnection.GetConnection();

        public BlobService(BlobServiceClient blobServiceClient)
        {
            _blobServiceClient = blobServiceClient;
        }

        public async Task<BlobInfo> GetBlobAsync(string name)
        {
            var containerClient = _blobServiceClient.GetBlobContainerClient("teamtra-blob");
            var blobClient = containerClient.GetBlobClient(name);
            var blobDownloadInfo = await blobClient.DownloadAsync();
            return new BlobInfo(blobDownloadInfo.Value.Content, blobDownloadInfo.Value.ContentType);
        }

        public IEnumerable<string> GetForCourse(string name)
        {
            var containerClient = _blobServiceClient.GetBlobContainerClient("teamtra-blob");
            var items = new List<string>();

            foreach (var blobItem in containerClient.GetBlobs())
            {
                if(blobItem.Name.Contains(name))
                {
                    items.Add(blobItem.Name);
                }    
            }

            return items;
        }

        public IEnumerable<string> ListBlobsAsync()
        {
            var containerClient = _blobServiceClient.GetBlobContainerClient("teamtra-blob");
            var items = new List<string>();

            foreach (var blobItem in containerClient.GetBlobs())
            {
                items.Add(blobItem.Name);
            }

            return items;
        }

        public async Task UploadFileBlobAsync(string filePath, string fileName, string userID)
        {
            Guid tokenpath = Guid.NewGuid();
            SqlCommand profile = new SqlCommand(@"update [User] set profilePicturePath = @ppp where userID=@id", dbConnection);
            dbConnection.Open();
            profile.Parameters.AddWithValue("id", userID);
            profile.Parameters.AddWithValue("ppp", "https://sapftnfilestorage.blob.core.windows.net/teamtra-blob/" + fileName + tokenpath);
            profile.ExecuteNonQuery();
            dbConnection.Close();
            var containerClient = _blobServiceClient.GetBlobContainerClient("teamtra-blob");
            var blobClient = containerClient.GetBlobClient(fileName + tokenpath);
            await blobClient.UploadAsync(filePath, new BlobHttpHeaders { ContentType = filePath.GetContentType() });
        }

        public void UploadContentBlobAsync(string content, string fileName)
        {
            var containerClient = _blobServiceClient.GetBlobContainerClient("teamtra-blob");
            var blobClient = containerClient.GetBlobClient(fileName);
            var bytes = Encoding.UTF8.GetBytes(content);
            var memoryStream = new MemoryStream(bytes);
            blobClient.UploadAsync(memoryStream, new BlobHttpHeaders { ContentType = fileName.GetContentType() });
        }

        public async Task DeleteBlobAsync(string blobName)
        {
            var containerClient = _blobServiceClient.GetBlobContainerClient("teamtra-blob");
            var blobClient = containerClient.GetBlobClient(blobName);
            await blobClient.DeleteIfExistsAsync();
        }
    }
}