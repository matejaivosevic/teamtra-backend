﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Teamtra_Backend.Hubs;
using Teamtra_Backend.Models;
using Teamtra_Backend.Services;

namespace Teamtra_Backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.WithOrigins("http://localhost:4200")
                       .AllowAnyMethod()
                       .AllowAnyHeader()
                        .AllowCredentials();
        }));
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));

            services.AddSingleton(x =>
              new BlobServiceClient("DefaultEndpointsProtocol=https;AccountName=sapftnfilestorage;AccountKey=ropkoVCSFv7n18lZr/LBUfrsd4V70xNJHmn/ckl0oCgxGoQilHjslG0U+JWO6yR1XRAqe6paQvZXlsOn+ugebA==;EndpointSuffix=core.windows.net"));

            services.AddSingleton<IBlobService, BlobService>();

            services.AddSignalR();

            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ITokenValidation, TokenValidation>();
            services.AddScoped<IMatchService, MatchService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors("MyPolicy");

            app.UseHttpsRedirection();
            app.UseMvc();

            app.UseSignalR(route =>
            {
                route.MapHub<QueueHub>("/teamtramatchesqueuehub");
            });
        }
    }
}
