﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Teamtra_Backend.Hubs;
using Teamtra_Backend.Models;
using Teamtra_Backend.Services;

namespace Teamtra_Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MatchController : ControllerBase
    {
        public IMatchService _matchService;
        private IHubContext<QueueHub> _hub;

        public MatchController(IMatchService service, IHubContext<QueueHub> hub)
        {
            _matchService = service;
            _hub = hub;
        }

        [HttpPost("creatematch")]
        public IActionResult CreateMatch([FromBody] Match match)
        {
            _matchService.CreateMatch(match);
            IEnumerable<Match> matches = _matchService.FilterBySportAndDifficulty(match.sport, match.category, match.city);
            _hub.Clients.All.SendAsync("teamtramatchesqueuehubsubscription", matches);
            return Ok();
        }

        [HttpGet("match/{id}")]
        public IActionResult getById(string id)
        {
            return Ok(_matchService.findMatchById(id));
        }

        [HttpPut("update")]
        public IActionResult update([FromBody] Match match)
        {
            return Ok(_matchService.UpdateMatch(match));
        }

        [HttpGet("autofilter")]
        public IActionResult autofilter(string sport, string category, string city)
        {
            return Ok(_matchService.FilterBySportAndDifficulty(sport, category, city));
        }

        [HttpPost("addtomatch")]
        public IActionResult AddUserToMatchQueue([FromBody] UserPlaysMatch body)
        {
            IEnumerable<Match> returnStatement = _matchService.AddUserToMatch(body.matchID, body.userID);
            _hub.Clients.All.SendAsync("teamtramatchesqueuehubsubscription", returnStatement);
            return Ok();
        }

        [HttpPost("cancel")]
        public IActionResult Cancel([FromBody] UserPlaysMatch body)
        {
            IEnumerable<Match> returnStatement = _matchService.CancelQueue(body.matchID, body.userID);
            _hub.Clients.All.SendAsync("teamtramatchesqueuehubsubscription", returnStatement);
            return Ok();
        }

        [HttpGet("onqueue/{userid}")]
        public IActionResult OnQueue(string userid)
        {
            return Ok(_matchService.AppliedMatches(userid));
        }
    }
}