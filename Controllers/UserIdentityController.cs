﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Teamtra_Backend.Models;
using Teamtra_Backend.Services;

namespace Teamtra_Backend.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("MyPolicy")]
    [ApiController]
    public class UserIdentityController : ControllerBase
    {
        private IUserService _userService;
        private ITokenValidation tokenService;
        public static readonly string AUTH = "Authorization";

        public UserIdentityController(IUserService userService, ITokenValidation token)
        {
            _userService = userService;
            tokenService = token;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody] Login loginParam)
        {
            var token = _userService.Authenticate(loginParam.email, loginParam.password);

            if (token == null)
                return BadRequest(new { message = "Email or password is incorrect" });

            return Ok(token);
        }

        [HttpGet("finduser/{userID}")]
        public IActionResult findUserById(string userID)
        {
            return Ok(this._userService.findUserByID(userID));
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(Register register)
        {
            Register r = _userService.Register(register);
            if (r != null)
            {
                await SendConfirmationEmailAsync(r);
                return Ok();
            }
            return BadRequest();
        }

        public async Task SendConfirmationEmailAsync(Register r)
        {
            var message = new MailMessage();
            message.To.Add(new MailAddress(r.email));
            message.From = new MailAddress("teamtracomunity@gmail.com");
            message.Subject = "Teamtra Confirmation E-mail";

            message.Body = string.Format("Welcome to Teamtra." +
                                         "This is your confirmation email. Please confirm your account on this <a href=\"" + "http://localhost:4200/accountconfirmationpage/" + r.regid + "/" + r.token + "\">link</a>");
            message.IsBodyHtml = true;

            using (var smtp = new SmtpClient())
            {
                smtp.UseDefaultCredentials = false;
                smtp.EnableSsl = true;
                var credential = new NetworkCredential
                {
                    UserName = "teamtracomunity@gmail.com",
                    Password = "MatejaIvosevic97."
                };
                smtp.Credentials = credential;
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                await smtp.SendMailAsync(message);
            }
        }

        [HttpPost("verifyaccount")]
        public IActionResult VerifyAccount([FromBody] VerifyAccount acc)
        {
            AccResponse response = new AccResponse();
            if (_userService.VerificationTokenValid(acc.userID, acc.token))
            {
                response.response = "success";
                return Ok(response);
            }
            response.response = "invalid token";
            return BadRequest(response);
        }

        [HttpGet("getrole")]
        public IActionResult getRole()
        {
            var req = Request.Headers[AUTH];
            if (!tokenService.isValid(req))
            {
                return Unauthorized();
            }
            var tokenHandler = new JwtSecurityTokenHandler();
            var tok = req.ToString().Split(" ")[1];
            var paresedToken = tokenHandler.ReadJwtToken(tok);

            var role = paresedToken.Claims
                .Where(c => c.Type == "role")
                .FirstOrDefault();

            return Ok(role);
        }

        [HttpGet("getid")]
        public IActionResult getId()
        {
            var req = Request.Headers[AUTH];
            if (!tokenService.isValid(req))
            {
                return Unauthorized();
            }
            var tokenHandler = new JwtSecurityTokenHandler();
            var tok = req.ToString().Split(" ")[1];
            var paresedToken = tokenHandler.ReadJwtToken(tok);

            var id = paresedToken.Claims
                .Where(c => c.Type == "id")
                .FirstOrDefault();

            return Ok(paresedToken);
        }

        [HttpPut("update")]
        public IActionResult Update(User user)
        {
            _userService.Update(user);
            return Accepted();
        }
    }
}
