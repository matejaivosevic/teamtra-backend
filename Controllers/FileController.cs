﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Teamtra_Backend.Models;
using Teamtra_Backend.Services;

namespace Teamtra_Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        private readonly IBlobService _blobService;
        private static readonly string _storeName = Path.Combine("wwwroot", "documents");
        private static readonly string _storePath = Path.Combine(Directory.GetCurrentDirectory(), _storeName);

        public FileController(IBlobService blobService)
        {
            _blobService = blobService;
        }

        [HttpGet("{blobName}")]
        public async Task<IActionResult> GetBlob(string blobName)
        {
            var data = await _blobService.GetBlobAsync(blobName);
            return File(data.Content, data.ContentType);
        }

        [HttpGet("list")]
        public IActionResult ListBlobs()
        {
            return Ok(_blobService.ListBlobsAsync());
        }

        [HttpGet("list/{course}")]
        public IActionResult ListBlobsForCourse(string course)
        {
            return Ok(_blobService.GetForCourse(course));
        }

        /*[HttpPost("uploadfile")]
        public async Task<IActionResult> UploadFile([FromBody] UploadFileRequest request)
        {
            await _blobService.UploadFileBlobAsync(request.FilePath, request.FileName);
            return Ok();
        }*/

        [HttpPost("uploadcontent")]
        public IActionResult UploadContent([FromBody] UploadContentRequest request)
        {
            _blobService.UploadContentBlobAsync(request.Content, request.FileName);
            return Ok();
        }

        [HttpDelete("{blobName}")]
        public async Task<IActionResult> DeleteFile(string blobName)
        {
            await _blobService.DeleteBlobAsync(blobName);
            return Ok();
        }

        [HttpPost, DisableRequestSizeLimit]
        [Route("add/{filenamefordoc}/{userid}")]
        public IActionResult Add(string filenamefordoc, string userid)
        {

            var file = Request.Form.Files[0];

            if (file == null || file.Length == 0)
                return BadRequest();

            // Velicina fajla veca od 5 MB
            if (file.Length > 5000000)
                return StatusCode(StatusCodes.Status405MethodNotAllowed);

            var fileExtension = Path.GetExtension(file.FileName);

            if (!Directory.Exists(_storePath))
            {
                Directory.CreateDirectory(_storePath);
            }

            string filePath = Path.Combine(_storePath) + fileExtension;

            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                file.CopyTo(fileStream);
            }

            string location = Path.Combine(filePath);
            _blobService.UploadFileBlobAsync(location, filenamefordoc, userid);

            return Ok();
        }
    }
}