﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;

namespace Teamtra_Backend.utils
{
    public static class Utils
    {
        public static List<T> ConvertDataTableToList<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public static T ConvertDataTableToModel<T>(DataTable dt)
        {
            if (dt.Rows.Count < 1)
            {
                return default(T);
            }
            T item = GetItem<T>(dt.Rows[0]);
            return item;
        }
        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName].ToString(), null);
                    else
                        continue;
                }
            }
            return obj;
        }

        public static string EncryptUserPassword(string originalPassword)
        {
            string encryptedPassword;
            using (SHA1Managed sha = new SHA1Managed())
            {
                var hashed = sha.ComputeHash(Encoding.UTF8.GetBytes(originalPassword));
                var builder = new StringBuilder(hashed.Length * 2);

                foreach (byte b in hashed)
                {
                    builder.Append(b.ToString("x2"));
                }
                return encryptedPassword = builder.ToString();
            }
        }
    }
}
