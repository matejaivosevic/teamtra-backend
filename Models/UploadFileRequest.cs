namespace Teamtra_Backend.Models
{
    public class UploadFileRequest
    {
        public string FilePath { get; set; }

        public string FileName { get; set; }
    }
}