﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Teamtra_Backend.Models
{
    public class SportDifficultyFilter
    {
        public string sport { get; set; }
        public string category { get; set; }
        public string city { get; set; }
    }
}
