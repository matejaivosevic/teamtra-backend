﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Teamtra_Backend.Models
{
    public class Match
    {
        public string matchID { get; set; }
        public string name { get; set; }
        public string place { get; set; }
        public string startingAt { get; set; }
        public string endingAt { get; set; }
        public string isPublic { get; set; }
        public string description { get; set; }
        public string requiredNumberOfPlayers { get; set; }
        public string availableNumberOfPlayers { get; set; }
        public string matchOwnerID { get; set; }
        public string winnerID { get; set; }
        public string city { get; set; }
        public string category { get; set; }
        public string sport { get; set; }
        public string queueNumber { get; set; }

    }
}
