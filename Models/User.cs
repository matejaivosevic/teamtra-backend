﻿namespace Teamtra_Backend.Models
{
    public class User
    {
        public string userID { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string category { get; set; }
        public string isVerified { get; set; }
        public string role { get; set; }
        public string city { get; set; }
        public string phoneNumber { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string age { get; set; }
        public string profilePicturePath { get; set; }
    }
}
