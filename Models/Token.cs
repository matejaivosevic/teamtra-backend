﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Teamtra_Backend.Models
{
    public class Token
    {
        public string auth_token { get; set; }
        public User user { get; set; }
    }
}
