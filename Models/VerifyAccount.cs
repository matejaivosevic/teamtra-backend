﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Teamtra_Backend.Models
{
    public class VerifyAccount
    {
        public string userID { get; set; }
        public string token { get; set; }
    }
}
