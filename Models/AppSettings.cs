﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Teamtra_Backend.Models
{
    public class AppSettings
    {
        public string Secret { get; set; }
    }
}
