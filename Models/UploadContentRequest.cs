namespace Teamtra_Backend.Models
{
    public class UploadContentRequest
    {
        public string Content { get; set; }

        public string FileName { get; set; }
    }
}