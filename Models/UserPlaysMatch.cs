﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Teamtra_Backend.Models
{
    public class UserPlaysMatch
    {
        public string matchID { get; set; }
        public string userID { get; set; }
    }
}
